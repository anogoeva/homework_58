import React from 'react';
import './Modal.css';
import BackDrop from "../BackDrop/BackDrop";
import Button from "../Button/Button";

const Modal = (props) => {
    return (
        <>
            <BackDrop show={props.show} onClick={props.close}/>
            <div className="Modal"
                 style={{
                     transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
                     opacity: props.show ? '1' : '0',
                 }}>
                <h3>{props.title}</h3>
                <hr/>
                {props.children}
                <Button type="Danger" onClick={props.close}>X</Button>
            </div>
        </>
    );
};

export default Modal;