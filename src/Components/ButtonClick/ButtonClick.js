import React from 'react';
import './ButtonClick.css';

const ButtonClick = (props) => {
    return (
        <div>
            <button className="OrderButton" onClick={props.purchaseHandler}>{props.text}</button>
        </div>
    );
};

export default ButtonClick;