import React from 'react';
import BackDrop from "../UI/BackDrop/BackDrop";
import './bootstrap.min.css';
import './Alert.css';

const Alert = (props) => {
    return (
        <>
            <BackDrop show={props.show} onClick={props.close}/>
            <div className="Alert"
                 style={{
                     transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
                     opacity: props.show ? '1' : '0',
                 }}>
                <div className={['alert alert-', props.type].join('')} role="alert">
                    A simple primary alert—check it out!
                    {props.dismiss ? <div className="Danger" onClick={props.close}>X</div> : ""}
                </div>
            </div>
        </>
    );
};

export default Alert;