import './App.css';
import Modal from "./Components/UI/Modal/Modal";
import {useState} from "react";
import ButtonClick from "./Components/ButtonClick/ButtonClick";
import Alert from "./Components/Alert/Alert";

function App() {

    const [showAlert, setShowAlert] = useState(false);
    const alertHandler = () => {
        setShowAlert(true);
    };

    const alertCancelHandler = () => {
        setShowAlert(false)
    };

    const [showPurchaseModal, setShowPurchaseModal] = useState(false);
    const purchaseHandler = () => {
        setShowPurchaseModal(true);
    };

    const purchaseCancelHandler = () => {
        setShowPurchaseModal(false)
    };

    return (
        <div className="App">
            <Modal
                show={showPurchaseModal}
                close={purchaseCancelHandler}
                title="Some kinda modal title"
            >
                <p>This is modal content</p>
            </Modal>
            <ButtonClick purchaseHandler={purchaseHandler} text="TASK №1"/>
            <Alert
                show={showAlert}
                close={alertCancelHandler}
                type="primary"
                dismiss="dismiss"
            >This is a warning type alert
            </Alert>
            <Alert
                show={showAlert}
                close={alertCancelHandler}
                type="danger"
                dismiss="dismiss"
            >This is a warning type alert
            </Alert>
            <Alert
                show={showAlert}
                close={alertCancelHandler}
                type="warning"
                dismiss="dismiss"
            >This is a warning type alert
            </Alert>
            <Alert show={showAlert}
                   close={alertCancelHandler} type="success">This is a success type alert</Alert>
            <ButtonClick purchaseHandler={alertHandler} text="TASK №2"/>
        </div>
    );
}

export default App;
